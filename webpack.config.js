module.exports = {
    entry: {
        app: ["./app/components/Main.js"]
    },
    output: {
        path: './app/dist/',
        filename: "app.js"
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                loader: "babel-loader",
                query: {
                    presets: ['babel-preset-stage-1', 'react']
                }
            }
        ]
    }
};