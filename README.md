# Project setup for React&Redux using ES7 syntax

## Stack
--------
* **React**
* **Redux**
* **ECMAScript 7**
* **Babel6 (stage1)**
* **Webpack**

## Setup
--------
* ### NodeJS
	`curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
	sudo apt-get install -y nodejs`

* ### NPM - install / update
	`sudo npm install npm -g`

	#### Important
	If you receive `EACCES` errors when trying to install package do following.

	`sudo chown -R $(whoami) ~/.npm`

* ### Dependencies
	`npm install`

* ### Build
    `webpack -w` ( simply )
    
    or
    
    `webpack -wc --progress` ( prettier output with colors and progress )

* ### Check if it works
    open `templates/index.html`

    and you hopefully can see "React on board!" in your browser.

## Directory structure for application
--------
Official examples by **Dan Abramov**: 
* `components` for “dumb” React components unaware of Redux;
* `containers` for “smart” React components connected to Redux;
* `actions` for all action creators, where file name corresponds to part of the app;
* `reducers` for all reducers, where file name corresponds to state key;
* `store` for store initialization.

This works well for **small and mid-level size apps**.

[More about "smart" and "dumb" components.](https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0)

## React DevTools
--------
[Official Facebook's post about DevTools](https://fb.me/react-devtools)

### Direct links:
* [Google Chrome](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi)
* [Mozilla Firefox](https://addons.mozilla.org/en-US/firefox/addon/react-devtools/)

## Webstorm integration
--------

### Plugins
* Markdown support
* React-templates

### Settings

1. #### JSX
    * go into `Languages & Frameworks` -> `JavaScript`
    * change `JavaScript language version` to `JSX Harmony`
    * and mark `Prefer Strict mode`

2. #### Libraries
    Go into `Languages & Frameworks` -> `JavaScript` -> `Libraries` -> `Download`

    * React-Redux-DefinitelyTyped ( enchance code completion with typed parameter
    information - TypeScript definitions)

        choose `TypeScript community stubs` -> `react-redux-DefinitelyTyped` -> `Download and Install`
